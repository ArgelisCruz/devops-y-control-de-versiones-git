# Ejercicio 4
Comandos que utilizaremos: [Manual](Manual.md)

## Gestionando repositorios remotos
Git nos permite enlazar 1 o más repositorios en un mismo ambiente local. Para gestinar los repositorios que en estos momentos tenemos en nuestro ambiente, colocamos:

```
git remote -v
```
 
Recuerda, siempre puedes migrar tu repositorio de git solamente copiando el directorio donde se encuentra tu carpeta .git. Añadamos otra fuente que apunte a nuestro mismo servidor remoto para ver como funciona:

```
git remote add otroOrigin [URL_DE_TU_REPO]
```

Ahora podrás hacer sentencias fetch pull y demás utilizando "otroOrigin" como fuente de datos:

```
git fetch otroOrigin
```

Para eliminar este repositorio que acabamos de crear como prueba (ya que carece de sentido tener 2 origenes apuntando a un mismo repositorio, procederemos a eliminarlo no sin antes visualizarlo con el comando que arriba utilizamos:

```
git remote -v
git remote remove otroOrigin
```